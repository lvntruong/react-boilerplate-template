/**
 *
 * IndexPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { NavLink, Link } from 'react-router-dom';
import axios from 'axios';

import {
  Row,
  Col,
  Button,
  Nav,
  Navbar,
  NavItem,
  NavDropdown,
  MenuItem,
  Carousel,
  ButtonToolbar,
  DropdownButton,
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
  Modal,
  Radio,
  Form,
  ProgressBar,
} from 'react-bootstrap';

// import Aux from '../../hoc/Aux/Aux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectIndexPage from './selectors';
import reducer from './reducer';
import saga from './saga';

import imgCarousel from '../../images/carousel.png';

function FieldGroup({ id, label, help, ...props }) {
  return (
    <FormGroup controlId={id}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}

export class IndexPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    show: false,
    showData: false,
    stepPostJob: 0,
    name: '',
    telephone: '',
    email: '',
    data: '',
  }

  showModal = () => {
    this.setState({ show: true });
  };

  showModalData = () => {
    axios.get('https://34bzimwwda.execute-api.us-east-2.amazonaws.com/prod/categories')
    .then((response) => {
      console.log(response.data);
      this.setState({ data: response.data, showData: true });
    })
    .catch((error) => {
      console.log(error);
    });
  };

  hideModal = () => {
    this.setState({ show: false, stepPostJob: 0 });
  }

  hideModalData = () => {
    this.setState({ showData: false });
  }

  nextStep = () => {
    const stepPostJob = this.state.stepPostJob + 1;
    this.setState({ stepPostJob });
  }

  modelBodyCondition() {
    switch (this.state.stepPostJob) {
      case 0:
        return (
          <div>
            <h4>What service do you need?</h4>
            <FormGroup>
              <Radio name="radioGroup">Web Design</Radio>
              <Radio name="radioGroup">Photography</Radio>
              <Radio name="radioGroup">Web Development</Radio>
              <Radio name="radioGroup">Web Development</Radio>
            </FormGroup>
          </div>
        );
      case 1:
        return (
          <div>
            <h4>What is your estimated Budget?</h4>
            <Row>
              <Form inline>
                <FormGroup controlId="formInlineName">
                  <Radio name="radioGroup" inline>
                  Around
                  </Radio>
                </FormGroup>
                <FormGroup controlId="formInlineEmail">
                  <FieldGroup
                    id="formControlsText"
                    type="number"
                    placeholder="0"
                  />
                </FormGroup>
              </Form>
              <Col lg={6}></Col>
            </Row>
            <Row>
              <FormGroup controlId="formInlineName">
                <Radio name="radioGroup" inline>I dont Know</Radio>
              </FormGroup>
            </Row>
          </div>
        );
      case 2:
        return (
          <div>
            <div className="row">
              <div className="col"><span>Where do we send your quotations?</span></div>
            </div>
            <div className="row">
              <div className="col"><input value={this.state.name} onChange={(e) => this.setState({ name: e.target.value })} type="text" placeholder="Your Name" /></div>
              <div className="col"><input value={this.state.telephone} onChange={(e) => this.setState({ telephone: e.target.value })} type="tel" placeholder="Your Telephone" /></div>
            </div>
            <div className="row" style={{ padding: '5px 0px' }}>
              <div className="col"><input value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })} type="email" placeholder="Your Email" /></div>
              <div className="col"></div>
            </div>
          </div>
        );
      case 3:
        return (
          <div>
            <div className="row">
              <div className="col"><span>Where to meet?</span></div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="form-check"><label className="form-check-label">
                  <input className="form-check-input" type="radio" name="job_post_wizard_website_forwho" /> Skype</label></div>
              </div>
              <div className="col"><input type="text" placeholder="Your Skype ID" /></div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="form-check"><label className="form-check-label"><input className="form-check-input" type="radio" name="job_post_wizard_website_forwho" /> Face to Face</label></div>
              </div>
              <div className="col"><input type="search" placeholder="Search a location" /></div>
            </div>
          </div>
        );
      case 4:
        return (
          <div>
            <div className="row">
              <div className="col"><span>When to meet?</span></div>
            </div>
            <div className="row">
              <div className="col-lg-5">
                <div className="form-check"><label className="form-check-label"><input className="form-check-input" type="radio" /> Available date/time</label></div>
              </div>
              <div className="col-auto col-lg-5"><input className="form-control-sm" type="datetime-local" /></div>
            </div>
            <div className="row">
              <div className="col-lg-5">
                <div className="form-check"><label className="form-check-label"><input className="form-check-input" type="radio" /> Pick from my calendar</label></div>
              </div>
              <div className="col-lg-5"><input type="text" placeholder="e.g. https://calendly.com/crowdskills" /></div>
            </div>
            <div className="row">
              <div className="col">
                <div className="form-check"><label className="form-check-label"><input className="form-check-input" type="radio" /> We will arrange later</label></div>
              </div>
            </div>
          </div>
        );
      case 5:
        axios.post('https://34bzimwwda.execute-api.us-east-2.amazonaws.com/prod/categories', {
          name: this.state.name,
          telephone: this.state.telephone,
          email: this.state.email,
        })
        .then((response) => {
          console.log(response.data);
          this.setState({ stepPostJob: 0 });
        })
        .catch((error) => {
          console.log(error);
        });
        return <ProgressBar active now={45} />;
      default: return null;
    }
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>CrowdSkills</title>
          <meta name="description" content="Description of CrowdSkills" />
        </Helmet>
        <div className="header-blue">
          <Navbar default collapseOnSelect staticTop>
            <Navbar.Header>
              <Navbar.Brand>
                <NavLink to="/">CrowdSkills</NavLink>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              <Nav>
                <NavItem eventKey={1} href="#">Link</NavItem>
                <NavItem eventKey={2} href="#">Link</NavItem>
                <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
                  <MenuItem eventKey={3.1}>Action</MenuItem>
                  <MenuItem eventKey={3.2}>Another action</MenuItem>
                  <MenuItem eventKey={3.3}>Something else here</MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey={3.3}>Separated link</MenuItem>
                </NavDropdown>
              </Nav>
              <Nav pullRight>
                <NavItem eventKey={1} href="#">Login</NavItem>
                <NavItem eventKey={2} className="btn btn-light action-button">Join</NavItem>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
        <div id="page_home">
          <div className="container">
            <Row>
              <Col md={12}>
                <Row>
                  <Col md={12}>
                    <Carousel>
                      <Carousel.Item>
                        <img alt="enclave_test" src={imgCarousel} style={{ width: '100%' }} />
                        <Carousel.Caption>
                          <h3>First slide label</h3>
                          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                      </Carousel.Item>
                      <Carousel.Item>
                        <img alt="enclave_test" src={imgCarousel} style={{ width: '100%' }} />
                        <Carousel.Caption>
                          <h3>Second slide label</h3>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </Carousel.Caption>
                      </Carousel.Item>
                      <Carousel.Item>
                        <img alt="enclave_test" src={imgCarousel} style={{ width: '100%' }} />
                        <Carousel.Caption>
                          <h3>Third slide label</h3>
                          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </Carousel.Caption>
                      </Carousel.Item>
                    </Carousel>
                  </Col>
                </Row>
                <Row>
                  <Col md={6} lg={3}><span style={{ fontSize: '20px' }}>What Do you need?</span></Col>
                  <Col md={6} lg={3}>
                    <ButtonToolbar>
                      <DropdownButton title="Web Developer" id="dropdown-size-medium">
                        <MenuItem eventKey="1">Web Developer</MenuItem>
                        <MenuItem eventKey="2">Graphic Designer</MenuItem>
                        <MenuItem eventKey="3">Content Writer</MenuItem>
                      </DropdownButton>
                    </ButtonToolbar>
                  </Col>
                  <Col md={6} lg={3}><span>Where Do you need it?</span></Col>
                  <Col md={6} lg={3}>
                    <FieldGroup
                      id="formControlsText"
                      type="text"
                      label=""
                      placeholder="e.g. London"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={12}>
                    <iframe title="enclave_test" frameBorder="0" width="100%" height="400" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyCbdYf7txJDEaQ2Rqpxgbdo-_iuMB1mvdY&amp;q=London&amp;zoom=11"></iframe>
                  </Col>
                </Row>
                <Row>
                  <Col md={12}>
                    <Row style={{ padding: '15px 0px' }}>
                      <Col md={6} lg={6} lgOffset={3} >
                        <span style={{ fontSize: '24px', padding: '5px' }}>Post a job to reach freelancers</span>
                        <Button bsSize="large" bsStyle="primary" id="btn_get_quotes" onClick={this.showModal}>POST</Button>
                        <Button bsSize="large" bsStyle="primary" id="btn_get_quotes" onClick={this.showModalData}>View Data From AWS</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </div>
        <div className="footer-dark">
          <footer>
            <div className="container">
              <Row>
                <div className="col-sm-6 col-md-3 item">
                  <h3>Services</h3>
                  <ul>
                    <li><Link to="/">Web design</Link></li>
                    <li><Link to="/">Development</Link></li>
                    <li><Link to="/">Hosting</Link></li>
                  </ul>
                </div>
                <div className="col-sm-6 col-md-3 item">
                  <h3>About</h3>
                  <ul>
                    <li><Link to="/">Company</Link></li>
                    <li><Link to="/">Team</Link></li>
                    <li><Link to="/">Careers</Link></li>
                  </ul>
                </div>
                <div className="col-md-6 item text">
                  <h3>CrowdSkills Pte Ltd</h3>
                  <p>Praesent sed lobortis mi. Suspendisse vel placerat ligula. Vivamus ac sem lacus. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in justo.</p>
                </div>
                <div className="col item social"><Link to="/"><i className="icon ion-social-facebook"></i></Link><Link to="/"><i className="icon ion-social-twitter"></i></Link><Link to="/"><i className="icon ion-social-snapchat"></i></Link><Link to="/"><i className="icon ion-social-instagram"></i></Link></div>
              </Row>
              <p className="copyright">Company Name © 2017</p>
            </div>
          </footer>
        </div>
        <Modal
          show={this.state.show && this.state.stepPostJob !== 5}
          onHide={this.hideModal}
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg">Data from AWS</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            { this.modelBodyCondition() }
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.nextStep}>Continue</Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={this.state.showData}
          onHide={this.hideModalData}
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg">Reach Freelancers</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormGroup controlId="formControlsTextarea">
              <FormControl componentClass="textarea" value={JSON.stringify(this.state.data)} />
            </FormGroup>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

IndexPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  indexpage: makeSelectIndexPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'indexPage', reducer });
const withSaga = injectSaga({ key: 'indexPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(IndexPage);
